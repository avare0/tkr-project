create table users
(
    user_name  varchar(255) not null
        primary key,
    birth_date timestamp,
    name       varchar(255),
    password   varchar(255),
    role       integer,
    surname    varchar(255)
);

alter table users
    owner to postgres;

    create table refreshtoken
(
    id             bigint       not null
        primary key,
    expiry_date    timestamp    not null,
    token          varchar(255) not null
        constraint uk_or156wbneyk8noo4jstv55ii3
            unique,
    user_user_name varchar(255)
        constraint fk6sm47very8hg1fr0vm31upe0w
            references users
);

alter table refreshtoken
    owner to postgres;

    create table product
(
    id               integer not null
        primary key,
    category         integer,
    color            varchar(255),
    description      varchar(255),
    price            double precision,
    sex              integer,
    subcategory      varchar(255),
    seller_user_name varchar(255)
        constraint fk5n14icisid5utk23jfw7v781n
            references users
);

alter table product
    owner to postgres;

create table product_order
(
    id             integer not null
        primary key,
    delivery       integer,
    total_price    double precision,
    user_user_name varchar(255)
        constraint fk9e5ml35eweffqsjjocuwoapo0
            references users
);

alter table product_order
    owner to postgres;

    create table product_order_products
(
    product_order_id integer not null
        constraint fkaubxdrympxrs4luo67y4fnvcc
            references product_order,
    products_id      integer not null
        constraint uk_rf07haj98hdaq6crpo5ajq00w
            unique
        constraint fk9mm40keccrp4otjokqq5xio77
            references product
);

alter table product_order_products
    owner to postgres;

create table feedback
(
    id         integer      not null
        primary key,
    date       timestamp,
    rate       double precision,
    product_id integer      not null
        constraint fklsfunb44jdljfmbx4un8s4waa
            references product,
    sender     varchar(255) not null
        constraint fkrn0n3wa92ldhno50oehlb9s1j
            references users
);

alter table feedback
    owner to postgres;
insert into public.users (user_name, birth_date, name, password, role, surname)
values  ('admin', null, 'Nekit', '$2a$12$YUaHq2jtGdGcKXVOmuBsq.SOxXCB3I2RkRmS9RCxMkSp2zEAdc3oe', 2, 'Zuev'),
        ('customer', '2022-04-03 01:18:27.000000', 'Egor', '$2a$12$YUaHq2jtGdGcKXVOmuBsq.SOxXCB3I2RkRmS9RCxMkSp2zEAdc3oe', 0, 'Panafidin'),
        ('seller', '2018-04-03 01:18:29.000000', 'Sasha', '$2a$12$YUaHq2jtGdGcKXVOmuBsq.SOxXCB3I2RkRmS9RCxMkSp2zEAdc3oe', 1, 'Zak');

        insert into public.product (id, category, color, description, price, sex, subcategory, seller_user_name)
values  (2, 1, 'Без цвета', 'Надежный провод', 900, 0, 'Провода', 'admin'),
        (3, 1, 'Без цвета', 'Замечательный холодильник', 15000, 0, 'Холодильники', 'admin'),
        (4, 2, 'Белая', 'Прекрасная спортивная кофточка', 1000, 1, 'Спортивная одежда', 'admin'),
        (5, 2, 'Красный', 'Прекрасная спортивная кофточка', 1000, 2, 'Спортивная одежда', 'admin'),
        (6, 2, 'Черный', 'Красивая юбка', 1000, 2, 'Юбки', 'admin'),
        (7, 3, 'Синий', 'iphone 20', 60000, 0, 'Телефоны', 'admin'),
        (9, 5, 'Синий', 'Убодный стол', 15000, 0, 'Столы и стулья', 'admin'),
        (10, 6, 'Черный', 'Коврик для киберспорты', 1500, 0, 'Киберспорт', 'admin'),
        (8, 4, 'Белый', 'Крутой чайник', 900, 0, 'Чайники', 'admin'),
        (1, 2, 'Розовая', 'Красивая юбка', 600, 2, 'Юбки', 'admin');
        insert into public.product_order (id, delivery, total_price, user_user_name)
values  (1, 0, 5000, 'admin'),
        (2, 1, 16000, 'customer'),
        (3, 1, 3500, 'customer'),
        (4, 0, 900, 'seller');
        insert into public.product_order_products (product_order_id, products_id)
values  (1, 1),
        (2, 2),
        (3, 3),
        (4, 4),
        (4, 5);
insert into public.feedback (id, date, rate, product_id, sender)
values  (1, '2020-04-03 01:33:53.000000', 5, 1, 'customer'),
        (2, '2021-04-03 01:33:56.000000', 5, 2, 'customer'),
        (3, '2022-06-03 01:33:59.000000', 4, 3, 'seller');