FROM openjdk:17-jdk-alpine
ARG JAR_FILE=backend/target/*.jar
COPY ${JAR_FILE} app.jar
ENTRYPOINT ["java","-jar","/app.jar"]

#FROM maven:latest as build
##WORKDIR /backend
##RUN apk add --no-cache maven
#COPY backend/mvnw .
#COPY backend/.mvn .mvn
#COPY backend/pom.xml .
#COPY backend/src src
#
#RUN mvn install -DskipTests
#RUN mkdir -p target/dependency && (cd target/dependency; jar -xf ../*.jar)
#
#
#FROM openjdk:8-jdk-alpine
#
#COPY --from=build target/*.jar app.jar
#
#ENTRYPOINT ["java","-jar","/app.jar"]