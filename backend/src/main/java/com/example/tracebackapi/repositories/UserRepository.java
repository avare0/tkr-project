package com.example.tracebackapi.repositories;

import com.example.tracebackapi.model.user.Users;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.RestController;

@Component
@Repository
public interface UserRepository extends JpaRepository<Users, String> {
        Users findUsersByUserName(String username);
}
