package com.example.tracebackapi.model;

import com.example.tracebackapi.model.DTO.FeedbackDTO;
import com.example.tracebackapi.model.user.Users;
import lombok.*;

import javax.persistence.*;
import java.util.Date;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Feedback {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "product_id", nullable = false)
    private Product product;

    @ManyToOne
    @JoinColumn(name = "sender", nullable = false)
    private Users sender;

    private Date date;

    private Double rate;

    private String comment;

    public FeedbackDTO toDTO() {
        FeedbackDTO feedbackDTO = new FeedbackDTO();
        feedbackDTO.setId(id);
        feedbackDTO.setComment(comment);
        feedbackDTO.setRate(rate);
        feedbackDTO.setDate(date);
        feedbackDTO.setUserName(sender.getUserName());
        feedbackDTO.setProductId(product.getId());

        return feedbackDTO;
    }

}
