package com.example.tracebackapi.model.DTO;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProductDTOwithSeller extends ProductDTO {

    public ProductDTOwithSeller(ProductDTO productDTO) {
        this.id = productDTO.id;

        this.subcategory = productDTO.subcategory;

        this.category = productDTO.category;

        this.color = productDTO.color;

        this.price = productDTO.price;

        this.sex = productDTO.sex;

        this.description = productDTO.description;
    }


    private String sellerName;
}
