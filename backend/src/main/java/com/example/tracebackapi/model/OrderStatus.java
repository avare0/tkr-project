package com.example.tracebackapi.model;

public enum OrderStatus {
    INPROGRESS,
    PURCHASED
}
