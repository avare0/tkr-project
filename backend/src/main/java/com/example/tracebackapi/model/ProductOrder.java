package com.example.tracebackapi.model;

import com.example.tracebackapi.model.DTO.OrderDTO;
import com.example.tracebackapi.model.user.Users;
import lombok.*;

import javax.persistence.*;
import java.util.List;
import java.util.stream.Collectors;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProductOrder {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToMany
    private List<Product> products;

    @ManyToOne
    private Users user;

    private Delivery delivery;

    private OrderStatus orderStatus;

    private Double totalPrice;

    public OrderDTO toorderDTO() {
        OrderDTO orderDTO = new OrderDTO();

        orderDTO.setDelivery(delivery);
        orderDTO.setTotalPrice(totalPrice);
        orderDTO.setListOfProducts(products.stream().map(x->x.toDTO()).collect(Collectors.toList()));
        orderDTO.setId(id  );
        orderDTO.setOrderStatus(orderStatus);

        return orderDTO;
    }

}
