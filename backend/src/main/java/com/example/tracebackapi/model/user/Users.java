package com.example.tracebackapi.model.user;

import com.example.tracebackapi.model.DTO.UserDTO;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.Date;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Users {

    @Id
    private String userName;

    private String name;

    private String surname;

    private Date birthDate;

    private Role role;

    private String password;

    public UserDTO toDTO() {
        UserDTO user = new UserDTO();

        user.setUserName(this.userName);
        user.setPassword(this.password);
        user.setBirthDate(this.birthDate);
        user.setRole(this.role);
        user.setName(this.name);
        user.setSurname(this.surname);

        return user;
    }
}
