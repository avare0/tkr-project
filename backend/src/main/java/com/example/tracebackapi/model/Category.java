package com.example.tracebackapi.model;

public enum Category {
    BOOTS("BOOTS"),
    CLOTHES("CLOTHES");

    private String category;

    Category(String category) {
        this.category = category;
    }
}
