package com.example.tracebackapi.model;

import com.example.tracebackapi.model.DTO.ProductDTO;
import com.example.tracebackapi.model.DTO.ProductDTOwithSeller;
import com.example.tracebackapi.model.user.Users;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Category category;

    private String subcategory;

    private Double price;

    private String description;

    private Sex sex;

    private String color;

    @ManyToOne
    private Users seller;

    public ProductDTO toDTO() {
        ProductDTO product = new ProductDTO();
        product.setId(id);
        product.setCategory(category);
        product.setSubcategory(subcategory);
        product.setPrice(price);
        product.setDescription(description);
        product.setSex(sex);
        product.setColor(color);
        return product;
    }

    public ProductDTOwithSeller toDTOWithSeller() {
        ProductDTO productDTO = toDTO();
        ProductDTOwithSeller productDTOwithSeller = new ProductDTOwithSeller(productDTO);
        productDTOwithSeller.setSellerName(seller.getUserName());

        return productDTOwithSeller;
    }



}
