package com.example.tracebackapi.model;

public enum SortingDirection {
    ASC,
    DSC,
}
