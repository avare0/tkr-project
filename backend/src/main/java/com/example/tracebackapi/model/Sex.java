package com.example.tracebackapi.model;

public enum Sex {
    MALE("M"),
    FEMALE("W"),
    UNISEX("U");

    private final String sex;

    Sex(String sex) {
        this.sex = sex;
    }
}
