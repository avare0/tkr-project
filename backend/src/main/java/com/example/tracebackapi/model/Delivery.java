package com.example.tracebackapi.model;

public enum Delivery {
    COURIER("COURIER"),
    SELF("SELF");

    private final String delivery;

    Delivery(String delivery) {
        this.delivery = delivery;
    }
}
