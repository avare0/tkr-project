package com.example.tracebackapi.model.DTO;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@AllArgsConstructor
@Data
@NoArgsConstructor
public class FeedbackDTO {
    private Long id;

    private Long productId;

    private String userName;

    private double rate;

    private Date date;

    private String comment;
}
