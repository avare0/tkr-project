package com.example.tracebackapi.model.DTO;

import com.example.tracebackapi.model.user.Role;
import com.example.tracebackapi.model.user.Users;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import java.util.Date;

@Data
public class UserDTO {
    private String userName;

    private String name;

    private String surname;

    private Date birthDate;

    private Role role;

    private String password;

    public Users toUsersEntity() {
        Users user = new Users();

        user.setUserName(this.userName);
        user.setPassword(this.password);
        user.setBirthDate(this.birthDate);
        user.setRole(this.role);
        user.setName(this.name);
        user.setSurname(this.surname);

        return user;
    }
}
