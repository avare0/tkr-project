package com.example.tracebackapi.model.DTO;

import com.example.tracebackapi.model.Delivery;
import com.example.tracebackapi.model.OrderStatus;
import com.example.tracebackapi.model.ProductOrder;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Id;
import java.util.List;
import java.util.stream.Collectors;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class OrderDTO {

    private Long id;

    private Delivery delivery;

    private OrderStatus orderStatus;

    private List<ProductDTO> listOfProducts;

    double totalPrice;

    public ProductOrder toEntity() {
        ProductOrder productOrder = new ProductOrder();

        productOrder.setDelivery(delivery);
        double totalPrice = listOfProducts.stream().mapToDouble(ProductDTO::getPrice).sum();
        productOrder.setTotalPrice(totalPrice);
        productOrder.setProducts(listOfProducts.stream().map(x-> x.toEntity()).collect(Collectors.toList()));
        return productOrder;
    }
}
