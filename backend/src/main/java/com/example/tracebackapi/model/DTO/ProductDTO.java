package com.example.tracebackapi.model.DTO;

import com.example.tracebackapi.model.Category;
import com.example.tracebackapi.model.Product;
import com.example.tracebackapi.model.Sex;
import lombok.Data;

@Data
public class ProductDTO {
    protected Long id;

    protected Category category;

    protected String subcategory;

    protected Double price;

    protected String description;

    protected Sex sex;

    protected String color;

    public Product toEntity() {
        Product product = new Product();

        product.setCategory(category);
        product.setSubcategory(subcategory);
        product.setPrice(price);
        product.setDescription(description);
        product.setSex(sex);
        product.setColor(color);
        return product;
    }
}
