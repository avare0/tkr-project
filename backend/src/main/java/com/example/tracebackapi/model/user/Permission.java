package com.example.tracebackapi.model.user;

public enum Permission {
    ADMIN("admin"),
    SELLER("seller"),
    CUSTOMER("customer");

    private final String permission;

    Permission(String permission) {
        this.permission = permission;
    }

    public String getPermission() {
        return permission;
    }
}
