package com.example.tracebackapi.security;

import com.example.tracebackapi.model.user.Permission;
import com.example.tracebackapi.model.user.Role;
import com.example.tracebackapi.model.user.Users;
import com.example.tracebackapi.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.annotation.PostConstruct;

@Configuration
@EnableWebSecurity
@Profile({"prod", "dev"})
public class Security extends WebSecurityConfigurerAdapter {

    private final JwtConfigurer jwtConfigurer;
    private UserRepository userRepository;
    private PasswordEncoder passwordEncoder;


    @Autowired
    public Security(JwtConfigurer configurer, UserRepository userRepository, PasswordEncoder passwordEncoder) {
        this.jwtConfigurer = configurer;
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }



    private static final String[] AUTH_WHITELIST = {
            "/swagger-resources/**",
            "/swagger-ui.html",
            "/v2/api-docs",
            "/webjars/**"
    };

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .authorizeRequests()
                .antMatchers("/api/v1/user/*").hasAnyAuthority(Permission.ADMIN.getPermission())
                .antMatchers("/api/v1/user").hasAnyAuthority(Permission.ADMIN.getPermission())
                .antMatchers("api/v1/product").hasAnyAuthority(Permission.ADMIN.getPermission(), Permission.CUSTOMER.getPermission(), Permission.SELLER.getPermission())
                .antMatchers("api/v1/product/*").hasAnyAuthority(Permission.ADMIN.getPermission(), Permission.CUSTOMER.getPermission(), Permission.SELLER.getPermission())
                .antMatchers("api/v1/feedback").hasAnyAuthority(Permission.ADMIN.getPermission(), Permission.CUSTOMER.getPermission(), Permission.SELLER.getPermission())
                .antMatchers("api/v1/feedback/*").hasAnyAuthority(Permission.ADMIN.getPermission(), Permission.CUSTOMER.getPermission(), Permission.SELLER.getPermission())
                .antMatchers("api/v1/order").hasAnyAuthority(Permission.ADMIN.getPermission(), Permission.CUSTOMER.getPermission(), Permission.SELLER.getPermission())
                .antMatchers("api/v1/order/*").hasAnyAuthority(Permission.ADMIN.getPermission(), Permission.CUSTOMER.getPermission(), Permission.SELLER.getPermission())
                .antMatchers("auth/login").permitAll()
                .antMatchers("/swagger-resources/**").permitAll()
                .antMatchers("/swagger-ui.html").permitAll()
                .antMatchers("v2/api-docs").permitAll()
                .antMatchers("/webjars/**").permitAll()
                .and()
                .apply(jwtConfigurer).and().csrf().disable().cors();
    }

    @PostConstruct
    private void initAdmin(){
        Users user = new Users();
        user.setUserName("admin");
        user.setPassword(passwordEncoder.encode("admin"));
        user.setRole(Role.ADMIN);
        userRepository.save(user);
    }

    @Override
    @Bean
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

}