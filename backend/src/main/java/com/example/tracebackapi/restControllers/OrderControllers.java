package com.example.tracebackapi.restControllers;

import com.example.tracebackapi.model.DTO.OrderDTO;
import com.example.tracebackapi.model.ProductOrder;
import com.example.tracebackapi.repositories.OrderRepository;
import com.example.tracebackapi.restControllers.payload.AddCartRequest;
import com.example.tracebackapi.services.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("api/v1/order")
public class OrderControllers {

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private OrderService orderService;

    @GetMapping("all")
    public ResponseEntity<List<OrderDTO>> getAllOrders() {
        List<ProductOrder> all = orderRepository.findAll();
        List<OrderDTO> collect = all.stream().map(x -> x.toorderDTO()).collect(Collectors.toList());
        return new ResponseEntity<>(collect, HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<Void> createOrder() {
        orderService.createNewOrder();
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping("{id}")
    public ResponseEntity<OrderDTO> addToCart(@PathVariable Long id, @RequestBody AddCartRequest addCartRequest) {
        try {
            OrderDTO orderDTO = orderService.addToCard(id, addCartRequest.getProductId());
            return new ResponseEntity<>(orderDTO, HttpStatus.OK);
        }
        catch (EntityNotFoundException exception) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("{id}")
    public ResponseEntity<Void> discard(@PathVariable Long id) {
        try {
            orderService.delete(id);
            return new ResponseEntity<>( HttpStatus.OK);
        }
        catch (EntityNotFoundException exception) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("{id}/purchase")
    public ResponseEntity<OrderDTO> purchase(@PathVariable Long id) {
        try {
            orderService.purchase(id);
            return new ResponseEntity<>( HttpStatus.OK);
        }
        catch (EntityNotFoundException exception) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}
