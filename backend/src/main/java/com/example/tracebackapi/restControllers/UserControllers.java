package com.example.tracebackapi.restControllers;

import com.example.tracebackapi.model.DTO.UserDTO;
import com.example.tracebackapi.model.user.Role;
import com.example.tracebackapi.model.user.Users;
import com.example.tracebackapi.repositories.UserRepository;
import com.example.tracebackapi.services.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityNotFoundException;
import java.util.List;
@RestController
@RequestMapping("api/v1/user")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class UserControllers {

    @Autowired
    UserRepository userRepository;

    @Autowired
    private UsersService usersService;

    @PostMapping("test")
    public ResponseEntity<Void> createUsersTest() {
        Users users = new Users();
        users.setUserName("Ada");
        users.setName("Egor");
        users.setRole(Role.CUSTOMER);
        users.setSurname("Panafidin");
        users.setPassword("300800");

        userRepository.save(users);

        return new ResponseEntity(HttpStatus.ACCEPTED);
    }

    @PostMapping
    public ResponseEntity<Void> createUser(@RequestBody UserDTO userDTO) {

        usersService.saveUser(userDTO);
        return new ResponseEntity(HttpStatus.ACCEPTED);
    }

    @GetMapping("/{username}")
    public ResponseEntity<UserDTO> getUserById(@PathVariable String username) {
        try {
            Users userById = userRepository.getById(username);
            return new ResponseEntity<UserDTO>(userById.toDTO(), HttpStatus.OK);
        }
        catch (EntityNotFoundException entityNotFoundException) {
            return new ResponseEntity<UserDTO>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/{username}")
    public ResponseEntity<Void> deleteUser(@PathVariable String username) {
        try {
            userRepository.deleteById(username);
            return new ResponseEntity<>(HttpStatus.OK);
        }
        catch (EmptyResultDataAccessException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("all")
    public ResponseEntity<List<Users>> getAllUsers() {

        return new ResponseEntity(userRepository.findAll(), HttpStatus.OK);
    }
}
