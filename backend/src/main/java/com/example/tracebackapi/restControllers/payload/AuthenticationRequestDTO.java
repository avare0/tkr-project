package com.example.tracebackapi.restControllers.payload;

import lombok.Data;

@Data
public class AuthenticationRequestDTO {
    private String login;
    private String password;
}
