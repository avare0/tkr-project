package com.example.tracebackapi.restControllers;

import com.example.tracebackapi.model.DTO.FeedbackDTO;
import com.example.tracebackapi.restControllers.payload.FeedBackRequest;
import com.example.tracebackapi.services.FeedbackService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityNotFoundException;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("api/v1/feedback")
public class FeedbackControllers {


    @Autowired
    private FeedbackService feedbackService;

    @PostMapping("/add/{id}")
    public ResponseEntity<Void> addFeedback(@PathVariable Long productId, @RequestBody FeedBackRequest feedBackRequest) {
        UserDetails userDetails;
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        userDetails = (UserDetails) authentication.getPrincipal();
        String username = userDetails.getUsername();
        feedbackService.addFeedback(productId, username, feedBackRequest.getRate(), feedBackRequest.getComment());
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping("{id}")
    public ResponseEntity<FeedbackDTO> getFeedback(@PathVariable Long id) {
        try {
            FeedbackDTO byId = feedbackService.getById(id);
            return new ResponseEntity<>(byId, HttpStatus.OK);
        }
        catch (EntityNotFoundException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("{id}")
    public  ResponseEntity<Void> deleteFeedback(@PathVariable Long id) {

        try {
            feedbackService.deleteFeedBack(id);
        } catch (IllegalAccessException e) {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping("/showRate/{productId}")
    public ResponseEntity<Void> showProductRate(@PathVariable Long productId) {
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping("/showFeedbacks/{productid}")
    public ResponseEntity<Void> showProductFeedbacks(@PathVariable Long productid) {
        return new ResponseEntity<>(HttpStatus.OK);
    }

}
