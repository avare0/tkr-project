package com.example.tracebackapi.restControllers.payload;

import lombok.Data;

@Data
public class FeedBackRequest {

    private Double rate;
    private String comment;
}
