package com.example.tracebackapi.restControllers;

import com.example.tracebackapi.model.DTO.ProductDTO;
import com.example.tracebackapi.model.DTO.ProductDTOwithSeller;
import com.example.tracebackapi.model.Product;
import com.example.tracebackapi.model.SortingDirection;
import com.example.tracebackapi.repositories.ProductRepository;
import com.example.tracebackapi.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("api/v1/product")
public class ProductControllers {

    private ProductService productService;

    private ProductRepository productRepository;

    @Autowired
    public ProductControllers(ProductService productService, ProductRepository productRepository) {
        this.productService = productService;
        this.productRepository = productRepository;
    }

    @GetMapping("all")
    public ResponseEntity<List<ProductDTOwithSeller>> getAll() {
        List<Product> all = productRepository.findAll();
        List<ProductDTOwithSeller> collect = all.stream().map(x -> x.toDTOWithSeller()).collect(Collectors.toList());
        return  new ResponseEntity<>(collect, HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<Void>  saveProduct(@RequestBody ProductDTO productDTO) {
        productService.addProduct(productDTO);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @DeleteMapping("{id}")
    public ResponseEntity<ProductDTOwithSeller> deleteProduct(@PathVariable Long id) {
        try {
            Product byId = productRepository.getById(id);

            productRepository.deleteById(id);
            return new ResponseEntity<ProductDTOwithSeller>(byId.toDTOWithSeller(), HttpStatus.OK);
        }
        catch (EntityNotFoundException entityNotFoundException) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("{id}")
    public ResponseEntity<ProductDTOwithSeller> getProduct(@PathVariable Long id) {
        try {
            Product byId = productRepository.getById(id);

            productRepository.deleteById(id);
            return new ResponseEntity<ProductDTOwithSeller>(byId.toDTOWithSeller(), HttpStatus.OK);
        }
        catch (EntityNotFoundException | EmptyResultDataAccessException entityNotFoundException) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(method = RequestMethod.GET, value = "/filter")
    @ResponseBody
    public ResponseEntity<List<ProductDTOwithSeller>> getProduct(@RequestParam(value = "search") String search) {
        List<ProductDTOwithSeller> productDTOwithSellers = productService.filterBy(search);
        return new ResponseEntity<>(productDTOwithSellers, HttpStatus.OK);
    }


    @RequestMapping(method = RequestMethod.GET, value = "/sort")
    @ResponseBody
    public ResponseEntity<List<ProductDTOwithSeller>> sorting(@RequestParam(value = "field") String field, @RequestParam(value = "ordering") Integer number) {
        SortingDirection sortingDirection = SortingDirection.ASC;
        if (number == 1 ){
            sortingDirection = SortingDirection.DSC;
        }
        List<ProductDTOwithSeller> productDTOwithSellers = productService.sortBy(field, sortingDirection);
        return new ResponseEntity<>(productDTOwithSellers, HttpStatus.OK);
    }

}
