package com.example.tracebackapi.services;

import com.example.tracebackapi.model.DTO.FeedbackDTO;
import com.example.tracebackapi.model.Feedback;
import com.example.tracebackapi.model.Product;
import com.example.tracebackapi.model.user.Role;
import com.example.tracebackapi.model.user.Users;
import com.example.tracebackapi.repositories.FeedbackRepository;
import com.example.tracebackapi.repositories.ProductRepository;
import com.example.tracebackapi.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.nio.file.AccessDeniedException;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class FeedbackService {

    private FeedbackRepository feedbackRepository;

    private UserRepository userRepository;

    private ProductRepository productRepository;

    AuthoritiesChecker authoritiesChecker;

    @Autowired
    public FeedbackService(FeedbackRepository feedbackRepository, UserRepository userRepository, ProductRepository productRepository,
                           AuthoritiesChecker authoritiesChecker) {
        this.feedbackRepository = feedbackRepository;
        this.userRepository = userRepository;
        this.productRepository = productRepository;
        this.authoritiesChecker = authoritiesChecker;
    }

    public FeedbackDTO getById(Long id) {
        Feedback byId = feedbackRepository.getById(id);
        return byId.toDTO();
    }

    public void addFeedback(Long productId, String userName, Double rate, String comment) {
        try {
            Users usersByUserName = userRepository.findUsersByUserName(userName);
            Product product = productRepository.getById(productId);
            Feedback feedback = new Feedback();
            feedback.setDate(new Date());
            feedback.setProduct(product);
            feedback.setSender(usersByUserName);
            feedback.setRate(rate);
            feedback.setComment(comment);
            feedbackRepository.save(feedback);
        }
        catch (EntityNotFoundException e ) {
            throw new EntityNotFoundException();
        }
    }

    public void deleteFeedBack(Long feedBackId) throws IllegalAccessException {

        Feedback byId = feedbackRepository.getById(feedBackId);
        UserDetails userDetails;
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        userDetails = (UserDetails) authentication.getPrincipal();
        if (AuthoritiesChecker.checkIsAuthorized(Role.ADMIN) || byId.getSender().getUserName().equals(userDetails.getAuthorities())) {
            feedbackRepository.delete(byId);
        }
        else {
            throw new IllegalAccessException();
        }
    }

   public List<FeedbackDTO>  getAll() {
        return feedbackRepository.findAll().stream().map(x -> x.toDTO()).collect(Collectors.toList());
   }

   public List<FeedbackDTO> getAllByProductId(Long productId) {
        return null;
   }
}
