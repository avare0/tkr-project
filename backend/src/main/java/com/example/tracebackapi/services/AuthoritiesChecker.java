package com.example.tracebackapi.services;

import com.example.tracebackapi.model.user.Permission;
import com.example.tracebackapi.model.user.Role;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.Set;

@Component
public class AuthoritiesChecker {

    public static Boolean checkIsAuthorized(Role role) {
        UserDetails userDetails;
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        userDetails = (UserDetails) authentication.getPrincipal();
        Collection<? extends GrantedAuthority> authorities = userDetails.getAuthorities();
        Set<SimpleGrantedAuthority> authoritiesOfUser = role.getAuthorities();
        for (SimpleGrantedAuthority authoritie : authoritiesOfUser) {
            if (authorities.contains(authoritie)) {
                return true;
            }
        }

        return false;
    }
}
