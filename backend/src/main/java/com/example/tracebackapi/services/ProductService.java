package com.example.tracebackapi.services;

import com.example.tracebackapi.model.DTO.ProductDTO;
import com.example.tracebackapi.model.DTO.ProductDTOwithSeller;
import com.example.tracebackapi.model.Product;
import com.example.tracebackapi.model.SortingDirection;
import com.example.tracebackapi.repositories.ProductRepository;
import com.example.tracebackapi.repositories.Specification.ProductSpecificationBuilder;
import com.example.tracebackapi.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Service
public class ProductService {

    private ProductRepository productRepository;
    private UserRepository userRepository;

    @Autowired
    public ProductService(ProductRepository productRepository, UserRepository userRepository) {
        this.productRepository = productRepository;
        this.userRepository = userRepository;
    }

    public void addProduct(ProductDTO productDTO) {
        Product product = productDTO.toEntity();

        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        product.setSeller(userRepository.findUsersByUserName(username));

        productRepository.save(product);
    }

    public List<ProductDTOwithSeller> sortBy(String field, SortingDirection sortingDirection) {
        Sort.Direction direction = Sort.Direction.ASC;
        if (sortingDirection == SortingDirection.DSC) {
            direction = Sort.Direction.ASC;
        }
        List<Product> all = productRepository.findAll(Sort.by(direction, field));
        return all.stream().map(x -> x.toDTOWithSeller()).collect(Collectors.toList());
    }

    public List<ProductDTOwithSeller> filterBy(String search) {
        ProductSpecificationBuilder builder = new ProductSpecificationBuilder();
        Pattern pattern = Pattern.compile("(\\w+?)(:|<|>)(\\w+?),");
        Matcher matcher = pattern.matcher(search + ",");
        while (matcher.find()) {
            builder.with(matcher.group(1), matcher.group(2), matcher.group(3));
        }

        Specification<Product> spec = builder.build();
        List<Product> all = productRepository.findAll(spec);
        return all.stream().map(x -> x.toDTOWithSeller()).collect(Collectors.toList());
    }
}
