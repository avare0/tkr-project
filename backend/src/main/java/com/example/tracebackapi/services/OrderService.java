package com.example.tracebackapi.services;

import com.example.tracebackapi.model.DTO.OrderDTO;
import com.example.tracebackapi.model.DTO.ProductDTO;
import com.example.tracebackapi.model.Delivery;
import com.example.tracebackapi.model.OrderStatus;
import com.example.tracebackapi.model.Product;
import com.example.tracebackapi.model.ProductOrder;
import com.example.tracebackapi.repositories.OrderRepository;
import com.example.tracebackapi.repositories.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;

@Service
public class OrderService {

    private OrderRepository orderRepository;
    private ProductRepository productRepository;

    @Autowired
    public OrderService(OrderRepository orderRepository, ProductRepository productRepository) {
        this.orderRepository = orderRepository;
        this.productRepository = productRepository;
    }

    public ProductOrder createNewOrder() {
        ProductOrder productOrder = new ProductOrder();
        productOrder.setDelivery(Delivery.COURIER);
        productOrder.setTotalPrice(0.0);
        productOrder.setOrderStatus(OrderStatus.INPROGRESS);
        orderRepository.save(productOrder);
        return productOrder;
    }

    public OrderDTO addToCard(Long orderId, Long productId) {
        Product byId = productRepository.getById(productId);
        if (byId == null) {
            throw new EntityNotFoundException();
        }

        ProductOrder protudctOrder = orderRepository.getById(orderId);
        List<Product> products = protudctOrder.getProducts();
        products.add(byId);
        double totalPrice = products.stream().mapToDouble(Product::getPrice).sum();
        protudctOrder.setTotalPrice(totalPrice);
        orderRepository.save(protudctOrder);
        return orderRepository.getById(orderId).toorderDTO();
    }

    public void delete(Long orderId) {
        try {
            orderRepository.deleteById(orderId);
        }
        catch (EmptyResultDataAccessException | EntityNotFoundException exception) {
            throw  new EntityNotFoundException();
        }
    }
    public OrderDTO purchase(Long orderId) {
        try {
            ProductOrder byId = orderRepository.getById(orderId);
            byId.setOrderStatus(OrderStatus.PURCHASED);
            return byId.toorderDTO();
        }
        catch (EntityNotFoundException | EmptyResultDataAccessException exception) {
            throw new EntityNotFoundException();
        }
    }
}
