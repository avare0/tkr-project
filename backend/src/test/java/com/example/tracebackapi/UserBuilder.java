package com.example.tracebackapi;

import com.example.tracebackapi.model.user.Role;
import com.example.tracebackapi.model.user.Users;
import lombok.NoArgsConstructor;

import java.util.Date;

@NoArgsConstructor
public class UserBuilder {

    private String userName;

    private String name = "";

    private String surName = "";

    private Date date = new Date();

    private Role role;

    private String password;



    public UserBuilder withName(String name) {
        this.name = name;
        return this;
    }

    public UserBuilder withUserName(String userName) {
        this.userName = userName;
        return this;
    }


    public UserBuilder withRole(Role role) {
        this.role = role;
        return this;
    }

    public UserBuilder withPassword(String password) {
        this.password = password;
        return this;
    }

    public Users build() {
        return new Users(userName, name, surName, date, role, password);
    }

}
