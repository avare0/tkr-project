package com.example.tracebackapi;

import com.example.tracebackapi.model.Category;
import com.example.tracebackapi.model.DTO.ProductDTO;
import com.example.tracebackapi.model.Product;
import com.example.tracebackapi.model.SortingDirection;
import com.example.tracebackapi.repositories.ProductRepository;
import com.example.tracebackapi.repositories.UserRepository;
import com.example.tracebackapi.restControllers.ProductControllers;
import com.example.tracebackapi.security.JwtTokenFilter;
import com.example.tracebackapi.security.JwtTokenProvider;
import com.example.tracebackapi.security.Security;
import com.example.tracebackapi.services.ProductService;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.Test;
import org.mockito.BDDMockito;
import org.mockito.Mockito;
import org.mockito.internal.verification.VerificationModeFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;

import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(value = {ProductControllers.class, ProductService.class}, excludeAutoConfiguration = {Security.class})
@AutoConfigureMockMvc(addFilters = false)
@ActiveProfiles("test")
public class ProductControllerTests {


    @Autowired
    private MockMvc mvc;

    @MockBean
    private ProductService service;

    @MockBean
    private ProductRepository productRepository;

    @MockBean
    private JwtTokenFilter filter;

    @MockBean
    private UserRepository userRepository;

    @MockBean
    private JwtTokenProvider provider;



    @Test
    @WithMockUser
    public void whenPostProduct_thenCreateProduct() throws  Exception {
        ProductDTO productDTO = new ProductDTO();
        productDTO.setCategory(Category.BOOTS);
        productDTO.setPrice(12.0);
        ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
        mvc.perform(post("/api/v1/product").contentType(MediaType.APPLICATION_JSON).content(ow.writeValueAsBytes(productDTO))).andExpect(status().isOk());
        verify(service, VerificationModeFactory.times(1)).addProduct(Mockito.any());
    }

    @Test
    @WithMockUser
    public void givenProducts_whenGetProducts_thenReturnJsonArray() throws Exception {
        Product productDTO = new Product();
        productDTO.setDescription("aga");
        productDTO.setSeller( new UserBuilder().withName("alex").build());
        Product productDTO1 = new Product();
        productDTO1.setDescription("ogo");
        productDTO1.setSeller( new UserBuilder().withName("alex").build());


        BDDMockito.given(productRepository.findAll()).willReturn(Arrays.asList(productDTO,productDTO1));

        mvc.perform(get("/api/v1/product/all").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andExpect(jsonPath("$[0].description", CoreMatchers.is(productDTO.getDescription())))
                .andExpect(jsonPath("$[1].description", CoreMatchers.is(productDTO1.getDescription())));
        verify(productRepository, VerificationModeFactory.times(1)).findAll();
    }

    @Test
    @WithMockUser
    public void filterProductsTests() throws Exception {
        Product productDTO = new Product();
        productDTO.setDescription("aga");
        productDTO.setSeller( new UserBuilder().withName("alex").build());
        Product productDTO1 = new Product();
        productDTO1.setDescription("ogo");
        productDTO1.setSeller( new UserBuilder().withName("alex").build());

        BDDMockito.given(productRepository.findAll()).willReturn(Arrays.asList(productDTO,productDTO1));
        BDDMockito.given(service.filterBy(Mockito.any())).willReturn(Arrays.asList(productDTO.toDTOWithSeller()));

        mvc.perform(get("/api/v1/product/filter?search=description:aga").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andExpect(jsonPath("$[0].description", CoreMatchers.is(productDTO.getDescription())));
        verify(service, VerificationModeFactory.times(1)).filterBy(Mockito.any());
    }

    @Test
    @WithMockUser
    public void SortingProductsTests() throws Exception {
        Product productDTO = new Product();
        productDTO.setDescription("aga");
        productDTO.setPrice(12.0);
        productDTO.setSeller( new UserBuilder().withName("alex").build());
        Product productDTO1 = new Product();
        productDTO1.setDescription("ogo");
        productDTO1.setPrice(15.0);
        productDTO1.setSeller( new UserBuilder().withName("alex").build());

        BDDMockito.given(productRepository.findAll()).willReturn(Arrays.asList(productDTO,productDTO1));
        BDDMockito.given(service.sortBy("price", SortingDirection.DSC)).willReturn(Arrays.asList(productDTO1.toDTOWithSeller(), productDTO.toDTOWithSeller()));

        mvc.perform(get("/api/v1/product/sort?field=price&ordering=1").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andExpect(jsonPath("$[0].description", CoreMatchers.is(productDTO1.getDescription())));
        verify(service, VerificationModeFactory.times(1)).sortBy("price", SortingDirection.DSC);
    }

}
