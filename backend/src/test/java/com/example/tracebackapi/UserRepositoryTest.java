package com.example.tracebackapi;

import com.example.tracebackapi.model.user.Role;
import com.example.tracebackapi.model.user.Users;
import com.example.tracebackapi.repositories.UserRepository;
import com.example.tracebackapi.security.UserDetailsServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;


@DataJpaTest
public class UserRepositoryTest {

    @Autowired
    UserRepository usersRepository;
    UserDetailsServiceImpl userDetailsService;
    @BeforeEach
    public void setUp() {
        userDetailsService = new UserDetailsServiceImpl(usersRepository);
    }

    @Test
    public void userSavingTest() {
        UserBuilder userBuilder = new UserBuilder();
        Users user = userBuilder.withName("Zuev").withRole(Role.CUSTOMER).withPassword("123").withUserName("meltik").build();
        Assertions.assertDoesNotThrow(() -> usersRepository.save(user));
    }

    @Test
    public void userDeleteTest() {
        UserBuilder userBuilder = new UserBuilder();
        Users user = userBuilder.withName("Zuev").withRole(Role.CUSTOMER).withPassword("123").withUserName("meltik").build();
        usersRepository.save(user);
        Assertions.assertDoesNotThrow(() -> usersRepository.delete(user));
    }

    @Test
    public void userUpdateTest() {
        UserBuilder userBuilder = new UserBuilder();
        Users user = userBuilder.withName("Zuev").withRole(Role.CUSTOMER).withPassword("123").withUserName("meltik").build();
        usersRepository.save(user);
        Users meltik = usersRepository.findUsersByUserName("meltik");
        meltik.setName("Doctor");
        usersRepository.saveAndFlush(meltik);
        Assertions.assertEquals("Doctor", usersRepository.findUsersByUserName("meltik").getName());
    }
}
